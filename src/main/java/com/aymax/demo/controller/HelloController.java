package com.aymax.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired
    @PreAuthorize("hasRole('group1')")
    @RequestMapping("/group1")
    public String helloWorld() {
        return "Page accessible only from users with role : 'group1'";
    }

    @Autowired
    @PreAuthorize("hasRole('group2')")
    @RequestMapping("/group2")
    public String helloWorld2() {
        return "Page accessible only from users with role : 'group2'";
    }
}